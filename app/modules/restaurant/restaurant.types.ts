export interface restaurant {
    restaurantName: string;
    manager: Object;
    review: [{
      reviewerName: string;
      Food: number;
      Ambiance: number;
      Service: number;
      Cleanliness: number;
      Overall: number;
    }];
}

export interface User {
    role: string;
    username: string;
    password: string;
  }