import { Application, NextFunction, Request, Response, json} from "express";
import cors from "cors"
import { routes } from "./routes.data"


export const registerRoutes = (app: Application) => {
    app.use(json());
    app.use(cors())
    for(let route of routes) {
        app.use(route.path, route.router);
    }


}