import { restaurant } from "../restaurant/restaurant.types";
import { User } from "../restaurant/restaurant.types";

export const Data: restaurant[] = [
  {
    restaurantName: "Sangrilla",
    manager: { name: "Jay", id: "1" },
    review: [{
      reviewerName: "sanket",
      Food: 3,
      Ambiance: 4,
      Service: 2,
      Cleanliness: 1,
      Overall: 3,
    }],

  },
  {
    restaurantName: "gargi",
    manager: { name: "Rahul", id: "2" },
    review: [{
      reviewerName: "Rajesh",
      Food: 3,
      Ambiance: 0,
      Service: 2,
      Cleanliness: 1,
      Overall: 4,
    }],
  
  },
];

export const loginDetails: User[] = [
  {
    role: "admin",
    username: "sanket",
    password: "11",
  },
  {
    role: "manager",
    username: "jay",
    password: "11",
  },
  {
    role: "manager",
    username: "rahul",
    password: "11",
  },
];